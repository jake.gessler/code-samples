Below are notes pertaining to the structure of the code samples.


- Each folder in /front contains ts, html, and scss files for a specific component

- app.component.html is the html for the parent component of the application

- styles.scss contains global styles