<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PhysicalAssetRepository")
 * @UniqueEntity("assetNum");
 */
class PhysicalAsset extends PersistentEntity
{
    /**
     * @ORM\Column(type="string")
     */
    private $assetType;

    /**
     * @ORM\Column(type="string")
     */
    private $make;

    /**
     * @ORM\Column(type="string")
     */
    private $model;

    /**
     * @ORM\Column(type="string", unique=true, length=190)
     */
    private $assetNum;

    /**
    * @ORM\Column(type="string")
    */
    private $serial;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ManyToOne(targetEntity="Location")
     * @JoinColumn(name="location_id", referencedColumnName="id")
     */
    private $location;

    public function getAssetType()
    {
        return $this->assetType;
    }

    public function setAssetType($assetType): void
    {
        $this->assetType = $assetType;
    }

    public function getMake()
    {
        return $this->make;
    }

    public function setMake($make): void
    {
        $this->make = $make;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model): void
    {
        $this->model = $model;
    }

    public function getAssetNum()
    {
        return $this->assetNum;
    }

    public function setAssetNum($assetNum): void
    {
        $this->assetNum = $assetNum;
    }

    public function getSerial()
    {
        return $this->serial;
    }

    public function setSerial($serial): void
    {
        $this->serial = $serial;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function setLocation($location): void
    {
        $this->location = $location;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description): void
    {
        $this->description = $description;
    }

    public function getNotes()
    {
        return $this->notes;
    }

    public function setNotes($notes): void
    {
        $this->notes = $notes;
    }

}
