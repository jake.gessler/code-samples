<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LocationRepository")
 */
class Location extends PersistentEntity
{
    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * @ORM\Column(type="string")
     */
    private $address;

    /**
     * @ORM\Column(type="integer")
     */
    private $downSpeed;

    /**
     * @ORM\Column(type="integer")
     */
    private $upSpeed;

    /**
     * @ORM\Column(type="smallint")
     */
    private $hasStatic;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $external;

    /**
     * @ORM\Column(type="smallint")
     */
    private $hasRingCentral;

    /**
     * @ORM\Column(type="smallint")
     */
    private $hasRetailRadio;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $internetProviderName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $internetProviderPhone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $omniLocation;

    public function getInternetProviderName()
    {
        return $this->internetProviderName;
    }

    public function setInternetProviderName($internetProviderName): void
    {
        $this->internetProviderName = $internetProviderName;
    }

    public function getInternetProviderPhone()
    {
        return $this->internetProviderPhone;
    }

    public function setInternetProviderPhone($internetProviderPhone): void
    {
        $this->internetProviderPhone = $internetProviderPhone;
    }


    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getDownSpeed()
    {
        return $this->downSpeed;
    }

    public function setDownSpeed($downSpeed): void
    {
        $this->downSpeed = $downSpeed;
    }

    /**
     * @return mixed
     */
    public function getUpSpeed()
    {
        return $this->upSpeed;
    }

    /**
     * @param mixed $upSpeed
     */
    public function setUpSpeed($upSpeed): void
    {
        $this->upSpeed = $upSpeed;
    }

    public function getHasStatic()
    {
        return $this->hasStatic;
    }

    public function setHasStatic($hasStatic): void
    {
        $this->hasStatic = $hasStatic;
    }

    public function getExternal()
    {
        return $this->external;
    }

    public function setExternal($external): void
    {
        $this->external = $external;
    }

    public function getHasRingCentral()
    {
        return $this->hasRingCentral;
    }

    public function setHasRingCentral($hasRingCentral): void
    {
        $this->hasRingCentral = $hasRingCentral;
    }

    public function getHasRetailRadio()
    {
        return $this->hasRetailRadio;
    }

    public function setHasRetailRadio($hasRetailRadio): void
    {
        $this->hasRetailRadio = $hasRetailRadio;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address): void
    {
        $this->address = $address;
    }

    public function getOmniLocation()
    {
        return $this->omniLocation;
    }

    public function setOmniLocation($omniLocation): void
    {
        $this->omniLocation = $omniLocation;
    }

}
