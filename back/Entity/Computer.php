<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ComputerRepository")
 */
class Computer extends PhysicalAsset
{
    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $cpu;

    /**
     * @ORM\Column(type="string")
     */
    private $memory;

    /**
     * @ORM\Column(type="string")
     */
    private $storage;

    /**
     * @ORM\Column(type="string")
     */
    private $os;

    /**
     * @ORM\Column(type="smallint")
     */
    private $wifi;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $wirelessKey;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $wirelessMouse;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getCpu()
    {
        return $this->cpu;
    }

    public function setCpu($cpu): void
    {
        $this->cpu = $cpu;
    }

    public function getMemory()
    {
        return $this->memory;
    }

    public function setMemory($memory): void
    {
        $this->memory = $memory;
    }

    public function getStorage()
    {
        return $this->storage;
    }

    public function setStorage($storage): void
    {
        $this->storage = $storage;
    }

    public function getOs()
    {
        return $this->os;
    }

    public function setOs($os): void
    {
        $this->os = $os;
    }

    public function getWifi()
    {
        return $this->wifi;
    }

    public function setWifi($wifi): void
    {
        $this->wifi = $wifi;
    }

    public function getWirelessKey()
    {
        return $this->wirelessKey;
    }

    public function setWirelessKey($wirelessKey): void
    {
        $this->wirelessKey = $wirelessKey;
    }

    public function getWirelessMouse()
    {
        return $this->wirelessMouse;
    }

    public function setWirelessMouse($wirelessMouse): void
    {
        $this->wirelessMouse = $wirelessMouse;
    }


}
