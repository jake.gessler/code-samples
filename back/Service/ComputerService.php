<?php


namespace App\Service;


use App\Entity\Computer;
use App\Entity\Location;
use App\Entity\Mpos;
use Doctrine\ORM\EntityManagerInterface;

class ComputerService
{
    private $em;
    private $ss;
    private $physicalAssetService;

    public function __construct(EntityManagerInterface $em, SerializerService $ss, PhysicalAssetService $physicalAssetService)
    {
        $this->em = $em;
        $this->ss = $ss;
        $this->physicalAssetService = $physicalAssetService;
    }

    public function deleteComputer($computer)
    {

    }

    public function retrieveAll()
    {
        $computers = $this->em->getRepository(Computer::class)->findAll();

        return $this->ss->serialize($computers);
    }

    public function retrieveAllSnipe()
    {
        $assets = $this->physicalAssetService->getAssets();
        $computers = $this->sortComputers($assets);
        return $computers;
    }

    private function sortComputers($allAssets)
    {

        $computers = array_filter($allAssets, function($asset) {
            return ($asset['category']['name'] == "Desktop" || $asset['category']['name'] == "Laptop") == true;
        });

        $computerObjects = [];

        foreach ($computers as $key => $value)
        {
            $computer = new Computer();
            $computer->setId($computers[$key]['id']);
            $computer->setAssetNum($computers[$key]['asset_tag']);
            $computer->setName($computers[$key]['name']);
            $computer->setSerial($computers[$key]['serial']);
            $computer->setMake($computers[$key]['manufacturer']['name']);
            $computer->setModel($computers[$key]['model']['name']);
            $computer->setOs($computers[$key]['custom_fields']['OS']['value']);
            $computer->setCpu($computers[$key]['custom_fields']['CPU']['value']);
            $computer->setMemory($computers[$key]['custom_fields']['Memory']['value']);
            $computer->setStorage($computers[$key]['custom_fields']['Storage']['value']);
            $computer->setAssetType("computer");

            $locName = $computers[$key]['location']['name'];
            $location = $this->em->getRepository(Location::class)->findOneBy(["name"=>$locName]);
            $computer->setLocation($location);

            $computerObjects[] = $computer;
        }

        return $computerObjects;

    }

}