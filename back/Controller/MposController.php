<?php


namespace App\Controller;


use App\Service\MposService;
use App\Service\SerializerService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class MposController extends Controller
{
    /**
     * @Route("mpos/retrieveAllSnipe")
     */
    public function retrieveAllSnipe(MposService $mposService, SerializerService $ss)
    {

        $allAssets = $mposService->retrieveAllSnipe();

        return new Response($ss->serialize($allAssets));

    }

    /**
     * @Route("/mpos/update")
     */
    public function update(Request $request, MposService $mposService)
    {
        $inMposDevice = json_decode($request->getContent());
        $response = $mposService->update($inMposDevice);
        return new Response($response);
    }
}