<?php

namespace App\Controller;

use App\Entity\Location;
use App\Service\LocationService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Service\SerializerService;

//use Symfony\Component\Routing\Annotation\Route;


class LocationController extends Controller
{

    /**
     * @Route("/php")
     */
    public function index()
    {
        return new Response(phpinfo());
    }

    /**
     * @Route("/location/add")
     */
    public function add(SerializerService $serializerService)
    {
        $request = Request::createFromGlobals();
        $content = $request->getContent();

        $inLocation = json_decode($content);
        $location = new Location();
        $location->setName($inLocation->name);
        $location->setUpSpeed($inLocation->upSpeed);
        $location->setDownSpeed($inLocation->downSpeed);

        $em = $this->getDoctrine()->getManager();
        try {
            $em->persist($location);
            $em->flush();
        } catch (ORMException $e) {
            return new Response($e->getMessage());
        }

        $response = $serializerService->serialize($location);
        return new Response($response);


    }

    /**
     * @Route("/location/retrieve")
     */
    public function retrieve(LocationService $locationService)
    {
        $locations = $locationService->retrieveAll();
        return new Response($locations);
    }

    /**
     * @Route("/location/update")
     */
    public function update(LocationService $locationService, Request $request)
    {
        $content = $request->getContent();
        $inLocation = json_decode($content);
        $response = $locationService->update($inLocation);

        return $response;
    }

    /**
     * @Route("/location/delete")
     */
    public function delete(LocationService $locationService, Request $request)
    {
        $inLocation = json_decode($request->getContent());
        $response = $locationService->deleteLocation($inLocation);
        return new Response($response);
    }

    /**
     * @Route("/location/import")
     */
    public function import(LocationService $locationService, Request $request, EntityManagerInterface $em)
    {
        $json = file_get_contents("../locations.json");
        $locationsArray = json_decode($json, true);
        foreach ($locationsArray as $l)
        {
            $location = new Location();
            $location->setName($l["name"]);
            $location->setDownSpeed($l["downSpeed"]);
            $location->setUpSpeed($l["upSpeed"]);
            $location->setHasRetailRadio($l["hasRetailRadio"]);
            $location->setHasRingCentral($l["hasRingCentral"]);
            $location->setHasStatic($l["hasStatic"]);

            try {
                $em->persist($location);
                $em->flush();
            } catch (ORMException $e) {
                continue;
            }
        }


        return new Response(var_dump($locationsArray));
    }


}
