<?php

namespace App\Controller;

use App\Entity\Computer;
use App\Entity\Location;
use App\Entity\Mpos;
use App\Entity\NetworkDevice;
use App\Entity\PaymentDevice;
use App\Entity\PhysicalAsset;
use App\Service\PhysicalAssetService;
use App\Service\SerializerService;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\ORMException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PhysicalAssetController extends Controller
{
    /**
     * @Route("/asset/update")
     */
    public function update(SerializerService $ss, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $locRepo = $this->getDoctrine()->getRepository(Location::class);
        $assetRepo = $this->getDoctrine()->getRepository(PhysicalAsset::class);
        $content = $request->getContent();
        $inAsset = json_decode($content);

        //If id=0 create new asset, otherwise, fetch it from db
        if($inAsset->id === 0)
            $asset = new PhysicalAsset();
        else
            $asset = $assetRepo->findOneBy(["id"=>$inAsset->id]);


        $inLocation = $inAsset->location;
        $inLocation = $locRepo->findOneBy(["id"=>$inLocation->id]);

        $asset->setAssetType($inAsset->assetType);
        $asset->setMake($inAsset->make);
        $asset->setModel($inAsset->model);
        $asset->setSerial($inAsset->serial);
        $asset->setDescription($inAsset->description);
        $asset->setAssetNum($inAsset->assetNum);
        $asset->setNotes($inAsset->notes);
        $asset->setLocation($inLocation);


        try {
            $em->persist($asset);
            $em->flush();
        } catch (ORMException $e) {
            return new Response($e->getMessage());
        }

        $response = $ss->serialize($asset);
        return new Response($response);
    }

    /**
     * @Route("/asset/delete")
     */
    public function delete(Request $request, SerializerService $ss, PhysicalAssetService $assetService, EntityManagerInterface $em)
    {
        $inAsset = json_decode($request->getContent());
        $response = $assetService->deleteAsset($inAsset);
        return new Response($response);
    }

    /**
     * @Route("asset/retrieve")
     */
    public function retrieve(SerializerService $ss, PhysicalAssetService $assetService)
    {
        $allAssets = $assetService->retrieveAll();
        $response = $ss->serialize($allAssets);
        return new Response($response);
    }

    /**
     * @Route("asset/retrieveAllSnipe")
     */
    public function retrieveAllSnipe(PhysicalAssetService $physicalAssetService, SerializerService $ss)
    {

        $allAssets = $physicalAssetService->retrieveAllSnipe();

        return new Response($ss->serialize($allAssets));

    }

    /**
     * @Route("/asset/importComputers")
     */
    public function importComputers( Request $request, EntityManagerInterface $em)
    {
        $json = file_get_contents("../computers.json");
        $computersArray = json_decode($json, true);

        $locRepo = $this->getDoctrine()->getRepository(Location::class);
        $assetRepo = $this->getDoctrine()->getRepository(PhysicalAsset::class);

        foreach ($computersArray as $c) {
            $computer = new Computer();
            $computer->setName($c["name"]);
            $computer->setMake($c["make"]);
            $computer->setModel($c["model"]);
            $computer->setSerial($c["serial"]);
            $computer->setCpu($c["cpu"]);
            $computer->setMemory($c["ram"]);
            $computer->setOs($c["os"]);
            $computer->setAssetNum($c["assetNum"]);
            $computer->setStorage($c["storage"]);
            $computer->setWifi($c["wifi"]);
            $computer->setAssetType($c["assetType"]);

            $inLocation = $c["location"];
            $inLocation = $locRepo->findOneBy(["name"=>$inLocation]);

            $computer->setLocation($inLocation);

            if ($assetRepo->findOneBy(["assetNum"=>$computer->getAssetNum()]))
            {
                continue;
            }

            try {
                $em->persist($computer);

            } catch (ORMException $e) {
                return new Response($e->getMessage());
            } catch (UniqueConstraintViolationException $e)
            {
                echo $e;
                continue;
            }
        }

        try{
            $em->flush();
        } catch (UniqueConstraintViolationException $e)
        {
            return new Response($e->getMessage());
        } catch (ORMException $e) {
            return new Response($e->getMessage());
        }

        return new Response(json_encode("Done"));

    }

    /**
     * @Route("/asset/importMpos")
     */
    public function importMpos( Request $request, EntityManagerInterface $em)
    {
        $json = file_get_contents("../mpos.json");
        $mposArray = json_decode($json, true);

        $locRepo = $this->getDoctrine()->getRepository(Location::class);
        $assetRepo = $this->getDoctrine()->getRepository(PhysicalAsset::class);

        foreach ($mposArray as $m) {
            $mpos = new Mpos();
            $mpos->setName($m["name"]);
            $mpos->setMake($m["make"]);
            $mpos->setModel($m["model"]);
            $mpos->setSerial($m["serial"]);
            $mpos->setAssetNum($m["assetNum"]);
            $mpos->setAssetType($m["assetType"]);

            $inLocation = $m["location"];
            $inLocation = $locRepo->findOneBy(["name"=>$inLocation]);

            $mpos->setLocation($inLocation);

            if ($assetRepo->findOneBy(["assetNum"=>$mpos->getAssetNum()]))
            {
                continue;
            }

            try {
                $em->persist($mpos);

            } catch (ORMException $e) {
                return new Response($e->getMessage());
            } catch (UniqueConstraintViolationException $e)
            {
                echo $e;
                continue;
            }
        }

        try{
            $em->flush();
        } catch (UniqueConstraintViolationException $e)
        {
            return new Response($e->getMessage());
        } catch (ORMException $e) {
            return new Response($e->getMessage());
        }

        return new Response(json_encode("Done"));

    }

    /**
     * @Route("/asset/importPayment")
     */
    public function importPaymentDevices( Request $request, EntityManagerInterface $em)
    {
        $json = file_get_contents("../paymentDevices.json");
        $paymentArray = json_decode($json, true);

        $locRepo = $this->getDoctrine()->getRepository(Location::class);
        $assetRepo = $this->getDoctrine()->getRepository(PhysicalAsset::class);

        foreach ($paymentArray as $p) {
            $paymentDevice = new PaymentDevice();
            $paymentDevice->setMake($p["make"]);
            $paymentDevice->setModel($p["model"]);
            $paymentDevice->setSerial($p["serial"]);
            $paymentDevice->setAssetNum($p["assetNum"]);
            $paymentDevice->setAssetType($p["assetType"]);

            $inLocation = $p["location"];
            $inLocation = $locRepo->findOneBy(["name"=>$inLocation]);

            $paymentDevice->setLocation($inLocation);

            if ($assetRepo->findOneBy(["assetNum"=>$paymentDevice->getAssetNum()]))
            {
                continue;
            }

            try {
                $em->persist($paymentDevice);

            } catch (ORMException $e) {
                return new Response($e->getMessage());
            } catch (UniqueConstraintViolationException $e)
            {
                echo $e;
                continue;
            }
        }

        try{
            $em->flush();
        } catch (UniqueConstraintViolationException $e)
        {
            return new Response($e->getMessage());
        } catch (ORMException $e) {
            return new Response($e->getMessage());
        }

        return new Response(json_encode("Done"));

    }

    /**
     * @Route("/asset/importNetwork")
     */
    public function importNetworkDevices( Request $request, EntityManagerInterface $em)
    {
        $json = file_get_contents("../networkDevices.json");
        $networkArray = json_decode($json, true);

        $locRepo = $this->getDoctrine()->getRepository(Location::class);
        $assetRepo = $this->getDoctrine()->getRepository(PhysicalAsset::class);

        foreach ($networkArray as $n) {
            $networkDevice = new NetworkDevice();
            $networkDevice->setMake($n["make"]);
            $networkDevice->setModel($n["model"]);
            $networkDevice->setSerial($n["serial"]);
            $networkDevice->setAssetNum($n["assetNum"]);
            $networkDevice->setAssetType($n["assetType"]);
            $networkDevice->setDescription($n["description"]);
            $networkDevice->setExternal($n["external"]);
            $networkDevice->setInternal($n["internal"]);

            $inLocation = $n["location"];
            $inLocation = $locRepo->findOneBy(["name"=>$inLocation]);

            $networkDevice->setLocation($inLocation);

            if ($assetRepo->findOneBy(["assetNum"=>$networkDevice->getAssetNum()]))
            {
                continue;
            }

            try {
                $em->persist($networkDevice);

            } catch (ORMException $e) {
                return new Response($e->getMessage());
            } catch (UniqueConstraintViolationException $e)
            {
                echo $e;
                continue;
            }
        }

        try{
            $em->flush();
        } catch (UniqueConstraintViolationException $e)
        {
            return new Response($e->getMessage());
        } catch (ORMException $e) {
            return new Response($e->getMessage());
        }

        return new Response(json_encode("Done"));

    }

    /**
     * @Route("/asset/deleteNetworkDevices")
     */
    public function deleteNetworkDevices(EntityManagerInterface $em)
    {
        $paymentRepo = $this->getDoctrine()->getRepository(PaymentDevice::class);

        $networkDevices = $paymentRepo->findBy(
            ['assetType' => 'networkDevice']
        );

        foreach($networkDevices as $nd)
        {
            $em->remove($nd);
        }

        $em->flush();

        return new Response("Done");
    }
}
