<?php

namespace App\Controller;

use App\Entity\Computer;
use App\Entity\Location;
use App\Service\ComputerService;
use App\Service\PhysicalAssetService;
use App\Service\SerializerService;
use Doctrine\ORM\ORMException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ComputerController extends Controller
{
    /**
     * @Route("/computer/update", name="computer")
     */
    public function update(SerializerService $ss, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $locRepo = $this->getDoctrine()->getRepository(Location::class);
        $computerRepo = $this->getDoctrine()->getRepository(Computer::class);
        $content = $request->getContent();
        $inComputer = json_decode($content);

        //If id=0 create new asset, otherwise, fetch it from db
        if($inComputer->id === 0)
            $computer = new Computer();
        else
            $computer = $computerRepo->findOneBy(["id"=>$inComputer->id]);


        $inLocation = $inComputer->location;
        $inLocation = $locRepo->findOneBy(["id"=>$inLocation->id]);

        $computer->setAssetType($inComputer->assetType);
        $computer->setMake($inComputer->make);
        $computer->setModel($inComputer->model);
        $computer->setSerial($inComputer->serial);
        //$computer->setDescription($inComputer->description);
        $computer->setAssetNum($inComputer->assetNum);
        $computer->setWifi($inComputer->wifi);
        $computer->setOs($inComputer->os);
        $computer->setStorage($inComputer->storage);
        $computer->setName($inComputer->name);
        $computer->setMemory($inComputer->memory);
        $computer->setCpu($inComputer->cpu);
        $computer->setLocation($inLocation);
        $computer->setNotes($inComputer->notes);


        try {
            $em->persist($computer);
            $em->flush();
        } catch (ORMException $e) {
            return new Response($e->getMessage());
        }

        $response = $ss->serialize($computer);
        return new Response($response);
    }

    /**
     * @Route("/computer/test", name="test")
     */
    public function test()
    {
        $user = $this->getUser();
        return new  Response("<html><body>Test</body></html>");
    }

    /**
     * @Route("computer/retrieveAll")
     */
    public function retrieveAll(ComputerService $computerService)
    {
        $computers = $computerService->retrieveAll();
        return new Response($computers);
    }

    /**
     * @Route("computer/retrieveAllSnipe")
     */
    public function retrieveAllSnipe(ComputerService $computerService, SerializerService $ss)
    {

        $allAssets = $computerService->retrieveAllSnipe();

        return new Response($ss->serialize($allAssets));

    }
}
