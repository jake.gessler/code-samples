import { TestBed, inject } from '@angular/core/testing';

import { MposDeviceService } from './mpos-device.service';

describe('MposDeviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MposDeviceService]
    });
  });

  it('should be created', inject([MposDeviceService], (service: MposDeviceService) => {
    expect(service).toBeTruthy();
  }));
});
