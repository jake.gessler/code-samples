import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Computer} from "../computers/Computer";
import {Mpos} from "./Mpos";
import {environment} from "../../environments/environment";


@Injectable()
export class MposDeviceService {

    private apiUrl: String;

    constructor(private http: HttpClient)
    {
        this.apiUrl = environment.apiUrl;
    }

    RetrieveAll() {
        return this.http.get<Computer[]>(this.apiUrl + "mpos/retrieveAll");
    }



    Update(mpos: Mpos)
    {

        return this.http.post(this.apiUrl + "mpos/update", mpos);

    }

}
