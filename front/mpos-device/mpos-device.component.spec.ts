import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MposDeviceComponent } from './mpos-device.component';

describe('MposDeviceComponent', () => {
  let component: MposDeviceComponent;
  let fixture: ComponentFixture<MposDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MposDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MposDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
