import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Computer} from "../computers/Computer";
import {PhysicalAsset} from "./PhysicalAsset";

@Component({
  selector: 'app-physical-asset',
  templateUrl: './physical-asset.component.html',
  styleUrls: ['./physical-asset.component.scss']
})
export class PhysicalAssetComponent implements OnInit {

  constructor() { }

    @Input() otherAssets: PhysicalAsset[];
    @Input() showLocation: boolean;
    @Output() selectRowEvent = new EventEmitter();

    columnsToDisplay = ['description', 'assetNum', "make", "model"];

    selectedRowIndex = -1;

  ngOnInit() {

      if(this.showLocation)
          this.columnsToDisplay.unshift('location');
  }

    selectRow(row)
    {
        if (this.selectedRowIndex==row.id)
        {
            this.selectedRowIndex = -1;
            this.selectRowEvent.emit(null);
            return;
        }
        this.selectRowEvent.emit(row);
        this.selectedRowIndex = row.id;
    }

}
