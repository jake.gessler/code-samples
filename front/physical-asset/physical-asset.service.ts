import { Injectable } from '@angular/core';
import {Computer} from "../computers/Computer";
import {PhysicalAsset} from "./PhysicalAsset";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Mpos} from "../mpos-device/Mpos";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable()
export class PhysicalAssetService {

    private allAssets: PhysicalAsset[];
    private apiUrl: string;

    constructor(private http: HttpClient)
    {
        this.allAssets = [];
        this.apiUrl = environment.apiUrl;
    }


   getAllAssets(callback, reload)
   {
        if (this.allAssets.length == 0 || this.allAssets === undefined || reload == true)
            this.RetrieveAllFromServer().subscribe( data => {
                callback(data);
                this.allAssets = data;
                return;
            });
        else
            callback(this.allAssets);
/*
      let obs = new Observable((observer) =>
          {
              if (this.allAssets.length > 0)

                  observer.next(this.allAssets);

              else
              {
                  this.RetrieveAllSnipe().subscribe((data) =>
                  {
                      this.allAssets = data;
                      observer.next(data)
                  })

              }
              observer.complete();
          });
       return obs;*/

    }


    //Used for both updating and adding assets
    Update(asset: PhysicalAsset)
    {
        //Make API call to update database
        console.log("Asset Updated!!!");
        console.log(asset);

        return this.http.post(this.apiUrl + "asset/update", asset);

        /*      observable.subscribe(data => {
                  console.log(data);

              });*/
    }

    Delete(asset: PhysicalAsset)
    {
        return this.http.post(this.apiUrl + "asset/delete", asset);
    }

    RetrieveAllFromServer()
    {
        //Make API call to retrieve all assets
        return this.http.get<PhysicalAsset[]>(this.apiUrl + "asset/retrieve");

        /*  let observable = this.http.get<PhysicalAsset[]>("api/asset/retrieve");
         let allAssets = [];

      /*observable.subscribe(data => {
             //console.log(data);
             //console.log(data[0].assetNum);
             allAssets = data;
         });

         return allAssets;*/
    }


    RetrieveAllSnipe() {
        return this.http.get<PhysicalAsset[]>(this.apiUrl + "asset/retrieveAllSnipe");
    }

}
