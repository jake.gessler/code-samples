import { TestBed, inject } from '@angular/core/testing';

import { PhysicalAssetService } from './physical-asset.service';

describe('PhysicalAssetService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PhysicalAssetService]
    });
  });

  it('should be created', inject([PhysicalAssetService], (service: PhysicalAssetService) => {
    expect(service).toBeTruthy();
  }));
});
