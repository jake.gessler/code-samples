import { Location } from "../store/Location";

export class PhysicalAsset {
    id: number;
    make: string;
    model: string;
    assetNum: string;
    serial: string;
    location: Location;
    assetType: string;
    description: string;
    notes: string;

    //Default constructor
        constructor()
        {
            this.id = 0;
            this.make = "";
            this.model = "";
            this.assetNum = "";
            this.serial = "";
            this.description = "";
            this.notes = "";
        }
}
