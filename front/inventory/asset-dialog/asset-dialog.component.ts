import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {PhysicalAsset} from "../../physical-asset/PhysicalAsset";
import {Computer} from "../../computers/Computer";
import {NetworkDevice} from "../../network-device/NetworkDevice";
import {PhysicalAssetService} from "../../physical-asset/physical-asset.service";
import {ComputerService} from "../../computers/computer.service";
import {LocationService} from "../../location.service";
import {Location} from "../../store/Location";
import {NetworkDeviceService} from "../../network-device/network-device.service";
import {PaymentDevice} from "../../payment-device/PaymentDevice";
import {PaymentDeviceService} from "../../payment-device/payment-device.service";
import {MposDeviceService} from "../../mpos-device/mpos-device.service";
import {Mpos} from "../../mpos-device/Mpos";
import {NgForm} from "@angular/forms";
import {NotificationService} from "../../notification-service/notification.service";
import {PrinterService} from "../../printer/printer.service";
import {Printer} from "../../printer/Printer";

@Component({
  selector: 'app-asset-dialog',
  templateUrl: './asset-dialog.component.html',
  styleUrls: ['./asset-dialog.component.scss']
})
export class AssetDialogComponent implements OnInit {

    @Input() asset;
    @Input() locations;

    types =
        [
            {
                name: "Computer",
                value: 1
            },
            {
                name: "Network Device",
                value: 2
            },
            {
                name: "Payment Device",
                value: 3
            },
            {
                name: "MPOS",
                value: 4
            },
            {
                name: "Printer",
                value: 6
            },
            {
                name: "Other",
                value: 5
            }
        ];

    newAsset: PhysicalAsset;
    selectedType;
    assetService;
    isEdit;
    isLoading = false;


    constructor(public dialogRef: MatDialogRef<AssetDialogComponent>,
                private physicalAssetService: PhysicalAssetService,
                private computerService: ComputerService,
                private locationService: LocationService,
                private networkDeviceService: NetworkDeviceService,
                private paymentDeviceService: PaymentDeviceService,
                private mposDeviceService: MposDeviceService,
                private notificationService: NotificationService,
                private printerService: PrinterService,
                @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit()
  {

      this.locations = this.data.locations;
      this.asset = this.data.asset;
      this.isEdit = this.data.edit;
      if(this.isEdit)
          this.setService(this.asset.assetType);
  }

    onTypeChange(newValue)
    {
        switch(newValue)
        {
            case "Computer":
            {
                if (!this.isEdit)
                    this.asset = new Computer();
                this.asset.assetType = "computer";
                this.assetService = this.computerService;
                break;
            }
            case "Network Device":
            {
                if (!this.isEdit)
                    this.asset = new NetworkDevice();
                this.asset.assetType = "networkDevice";
                this.assetService = this.networkDeviceService;
                break;
            }

            case "Payment Device":
            {
                if (!this.isEdit)
                    this.asset = new PaymentDevice();
                this.asset.assetType = "paymentDevice";
                this.assetService = this.paymentDeviceService;
                break;
            }

            case "MPOS":
            {
                if (!this.isEdit)
                    this.asset = new Mpos();
                this.asset.assetType = "mposDevice";
                this.assetService = this.mposDeviceService;
                break;
            }

            case "Printer":
            {
                if (!this.isEdit)
                    this.asset = new Printer();
                this.asset.assetType = "printer";
                this.assetService = this.printerService;
                break;
            }
           default:
            {
                if (!this.isEdit)
                    this.asset = new PhysicalAsset();
                this.asset.assetType = "other";
                this.assetService = this.physicalAssetService;
                break;
            }
        }
    }

    onSubmit()
    {
        this.isLoading = true;
        this.assetService.Update(this.asset)
            .subscribe(
                (data) =>
                {
                    this.dialogRef.close(true);
                    let message = this.isEdit ? "Asset Updated!" : "Asset Added";
                    this.notificationService.showSnackBar(message, "OK", 2000);
                    this.isLoading = false;
                },  //success
                        error => {
                    console.log(error);
                    this.isLoading = false;
                }); //error
    }

    onCancel()
    {
        this.dialogRef.close(false);
    }


    compareLoc(loc1: Location, loc2: Location): boolean
    {
        return loc1 && loc2 ? loc1.id === loc2.id : loc1 === loc2;
    }

    setService(assetType) {

        switch (assetType) {
            case "computer": {
                this.assetService = this.computerService;
                break;
            }
            case "networkDevice": {
                this.assetService = this.networkDeviceService;
                break;
            }
            case "paymentDevice": {
                this.assetService = this.paymentDeviceService;
                break;
            }
            case "mposDevice": {
                this.assetService = this.mposDeviceService;
                break;
            }
            case "printer": {
                this.assetService = this.printerService;
            }
            default: {
                this.assetService = this.physicalAssetService;
                break;
            }
        }
    }

}
