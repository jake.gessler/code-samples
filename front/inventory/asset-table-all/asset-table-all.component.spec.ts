import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetTableAllComponent } from './asset-table-all.component';

describe('AssetTableAllComponent', () => {
  let component: AssetTableAllComponent;
  let fixture: ComponentFixture<AssetTableAllComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetTableAllComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetTableAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
