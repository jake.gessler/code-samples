import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {PhysicalAsset} from "../../physical-asset/PhysicalAsset";
import {NetworkDevice} from "../../network-device/NetworkDevice";

@Component({
  selector: 'app-asset-table-all',
  templateUrl: './asset-table-all.component.html',
  styleUrls: ['./asset-table-all.component.scss']
})
export class AssetTableAllComponent implements OnInit {

  constructor() { }

    @Input() assets: PhysicalAsset[];
    //@Input() showLocation: boolean;
    @Input() columnsToDisplay;
    @Input() selectable = true;
    @Output() selectRowEvent = new EventEmitter();

    @ViewChild(MatPaginator) paginator: MatPaginator;

    selectedRowIndex = -1;
    //columnsToDisplay = [];

/*    locColumn =
        {
            columnDef: "location",
            header: "Location",
            cell: (row) => `${row.location.name}`
        };*/

    dataSource = new MatTableDataSource();

    ngOnInit()
    {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sortingDataAccessor = (item: PhysicalAsset, property) =>
        {
            if(property == "location")
                return item.location.name;

            return item[property];
        };

        this.dataSource.sort = this.sort;
        this.dataSource.data = this.assets;
        this.dataSource.filterPredicate = this.tableFilter;
    }

  /*  ngAfterViewInit()
    {

    }*/

    ngOnChanges()
    {
/*        if(this.showLocation && !this.columns.some(e => e.columnDef == "location"))
            this.columns.unshift(this.locColumn);

        this.columnsToDisplay = [];

        if(this.columns)
        {
            for (let i = 0; i < this.columns.length; i++)
            {
                this.columnsToDisplay.push(this.columns[i].columnDef);
            }
        }*/

       if(this.assets && this.dataSource)
        {
            this.dataSource.data = this.assets;
        }

    }

    selectRow(row)
    {
        if (!this.selectable) return;

        if (this.selectedRowIndex==row.id)
        {
            this.selectedRowIndex = -1;
            this.selectRowEvent.emit(null);
            return;
        }
        this.selectRowEvent.emit(row);
        this.selectedRowIndex = row.id;
    }

    deselectRow()
    {
        this.selectedRowIndex = -1;
        //this.selectRowEvent.emit(null);
    }

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }

    tableFilter(data: PhysicalAsset, filter: string): boolean
    {
        let str = "";
        function test(obj) {

            Object.keys(obj).forEach(function (key) {
                if (typeof obj[key] === 'object' && obj[key] != null) {
                    test(obj[key]);
                }
                else
                    str = str.concat(obj[key]);
            });
        }
        test(data);

        let result = data.location.name.toLowerCase().indexOf(filter);
        return str.toLowerCase().indexOf(filter) != -1;

    }

    @ViewChild(MatSort) sort: MatSort;


}
