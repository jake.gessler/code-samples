import {Component, Input, OnChanges, OnInit, Pipe, PipeTransform, SimpleChanges, ViewChild} from '@angular/core';
import { NetworkDevice } from "../network-device/NetworkDevice";
import { Computer } from "../computers/Computer";
import { ComputerService } from "../computers/computer.service";
import { NetworkDeviceService } from "../network-device/network-device.service";
import { MatDialog } from "@angular/material";
import { AssetDialogComponent } from "./asset-dialog/asset-dialog.component";
import { PhysicalAssetService } from "../physical-asset/physical-asset.service";
import { PhysicalAsset } from "../physical-asset/PhysicalAsset";
import { LocationService} from "../location.service";
import { Location } from "../store/Location";
import {ConfirmDialogComponent} from "../confirm-dialog/confirm-dialog.component";
import {PaymentDevice} from "../payment-device/PaymentDevice";
import {Mpos} from "../mpos-device/Mpos";
import {AssetTableComponent} from "./asset-table/asset-table.component";
import {NotificationService} from "../notification-service/notification.service";
import {Printer} from "../printer/Printer";

@Component({
    selector: 'app-inventory',
    templateUrl: './inventory.component.html',
    styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit{

    @ViewChild(AssetTableComponent)
    private assetTableComponent: AssetTableComponent;

    selectedType = null;
    networkDevices: NetworkDevice[];
    computers: Computer[];
    paymentDevices: PaymentDevice[];
    mposDevices: Mpos[];
    otherAssets: PhysicalAsset[];
    printers: Printer[];
    filterString: string;
    selectedAsset = null;
    allAssets;
    allAssetArrays;
    currentAssets;
    columns;
    locations: Location[];
    assetsLoaded = false;
    locationsLoaded = false;
    columnsToDisplay = [];
    assetArray = [];

    types =
    [
        {
            name: "Computers",
            value: 1
        },
        {
            name: "Routers",
            value: 2
        },
        {
            name: "Payment Devices",
            value: 3
        },
        {
            name: "MPOS",
            value: 4
        },
        {
            name: "Printers",
            value: 6
        },
        {
            name: "Other",
            value: 5
        }
    ];

    typesObject =
        {
            all:
                {
                    displayName: "All",
                    value: 7,
                    name: "all"
                },
              computer:
                  {
                      displayName: "Computers",
                      value: 1,
                      name: "computer"
                  },
              networkDevice:
                  {
                      displayName: "Network Devices",
                      value: 2,
                      name: "networkDevice"
                  },
              paymentDevice:
                  {
                      displayName: "Payment Devices",
                      value: 3,
                      name: "paymentDevice"
                  },
              mposDevice:
                  {
                      displayName: "MPOS Devices",
                      value: 4,
                      name: "mposDevice"
                  },
            printer:
                {
                    displayName: "Printers",
                    value: 6,
                    name: "printer"
                },
              other:
                  {
                      displayName: "Other",
                      value: 5,
                      name: "other"
                  }
        };

    //Table Column Config
    computerColumns =
        [
            {
                columnDef: "assetNum",
                header: "Asset Number",
                cell: (row: Computer) => `${row.assetNum}`
            },
            {
                columnDef: "name",
                header: "Name",
                cell: (row: Computer) => `${row.name}`
            },
            {
                columnDef: "make",
                header: "Make",
                cell: (row: Computer) => `${row.make}`
            },
            {
                columnDef: "model",
                header: "Model",
                cell: (row: Computer) => `${row.model}`
            },
            {
                columnDef: "cpu",
                header: "CPU",
                cell: (row: Computer) => `${row.cpu}`
            },
            {
                columnDef: "memory",
                header: "Memory",
                cell: (row: Computer) => `${row.memory}`
            },
            {
                columnDef: "os",
                header: "OS",
                cell: (row: Computer) => `${row.os}`
            }
        ];

    networkDeviceColumns =
        [
                {
                    columnDef: "assetNum",
                    header: "Asset Number",
                    cell: (row: NetworkDevice) => `${row.assetNum}`
                },
                {
                    columnDef: "make",
                    header: "Make",
                    cell: (row: NetworkDevice) => `${row.make}`
                },
                {
                    columnDef: "model",
                    header: "Model",
                    cell: (row: NetworkDevice) => `${row.model}`
                },
                {
                    columnDef: "externalIP",
                    header: "External IP",
                    cell: (row: NetworkDevice) => `${row.external}`
                },
                {
                    columnDef: "internalIP",
                    header: "Internal IP",
                    cell: (row: NetworkDevice) => `${row.internal}`
                },

            ];

    paymentDeviceColumns =
        [
            {
                columnDef: "assetNum",
                header: "Asset Number",
                cell: (row: PaymentDevice) => `${row.assetNum}`
            },
            {
                columnDef: "make",
                header: "Make",
                cell: (row: PaymentDevice) => `${row.make}`
            },
            {
                columnDef: "model",
                header: "Model",
                cell: (row: PaymentDevice) => `${row.model}`
            }
        ];

    mposDeviceColumns =
        [
            {
                columnDef: "assetNum",
                header: "Asset Number",
                cell: (row: Mpos) => `${row.assetNum}`
            },
            {
                columnDef: "name",
                header: "Name",
                cell: (row: Mpos) => `${row.name}`
            },
            {
                columnDef: "make",
                header: "Make",
                cell: (row: Mpos) => `${row.make}`
            },
            {
                columnDef: "model",
                header: "Model",
                cell: (row: Mpos) => `${row.model}`
            },


        ];

    otherColumns =
        [
            {
                columnDef: "assetNum",
                header: "Asset Number",
                cell: (row: PhysicalAsset) => `${row.assetNum}`
            },
            {
                columnDef: "description",
                header: "Description",
                cell: (row: PhysicalAsset) => `${row.description}`
            },
            {
                columnDef: "make",
                header: "Make",
                cell: (row: PhysicalAsset) => `${row.make}`
            },
            {
                columnDef: "model",
                header: "Model",
                cell: (row: PhysicalAsset) => `${row.model}`
            }
        ];


    computerColumnsToDisplay = ['location', 'assetNum', 'name', 'make', 'model', 'cpu', 'mem', 'os', 'notes'];
    networkColumnsToDisplay = ['location', 'assetNum', 'make', 'model', 'external', 'internal'];
    paymentColumnsToDisplay = ['location', 'assetNum', 'make', 'model', 'serial'];
    mposColumnsToDisplay =  ['location', 'assetNum', 'name', 'make', 'model'];
    otherColumnsToDisplay = ['location', 'assetNum', 'description', 'make', 'model', 'notes'];
    printerColumnsToDisplay = ['location',  'assetNum', 'make', 'model', 'ipAddress'];

    constructor(private ComputerService: ComputerService,
                private NetworkDeviceService: NetworkDeviceService,
                public dialog: MatDialog,
                public PhysicalAssetService: PhysicalAssetService,
                public LocationService: LocationService,
                public notificationService: NotificationService
    ) { }

    ngOnInit() {

        this.selectedType = this.typesObject.computer;
        this.columnsToDisplay = this.computerColumnsToDisplay;
        this.computers = [];
        this.columns = this.computerColumns;
        this.loadAssets();
        this.loadLocations();

        //this.PhysicalAssetService.RetrieveAllSnipe();

        }

    //calls RetrieveAll from service and subscribes to the observable, sets allAssets to data from that observable stream
    //Snipe-IT version
/*    loadAssets()
    {

        this.PhysicalAssetService.getAllAssets(data =>
        {
            this.allAssets = data;
            this.filterAssets();
            this.onTypeChange(this.selectedType);
        });
    }*/

    loadAssets()
    {
        this.assetsLoaded = false;
        this.PhysicalAssetService.getAllAssets(data =>
        {
            this.allAssetArrays = data;
            this.allAssets = data.allAssets;
            //this.filterAssets();
            this.computers = data.computers;
            this.networkDevices = data.networkDevices;
            this.paymentDevices = data.paymentDevices;
            this.mposDevices = data.mpos;
            this.otherAssets = data.other;
            this.printers = data.printers;
            this.assetArray = this.assetArray.concat(this.computers, this.networkDevices, this.paymentDevices, this.mposDevices, this.otherAssets, this.printers);
            this.assetsLoaded = true;
            //this.onTypeChange(this.selectedType);
        }, false);
    }

    reloadAssets()
    {
        this.assetsLoaded = false;
        this.PhysicalAssetService.getAllAssets(data =>
        {
            this.allAssetArrays = data;
            this.allAssets = data.allAssets;
            //this.filterAssets();
            this.computers = data.computers;
            this.networkDevices = data.networkDevices;
            this.paymentDevices = data.paymentDevices;
            this.mposDevices = data.mpos;
            this.otherAssets = data.other;
            this.printers = data.printers;
            this.assetArray = this.assetArray.concat(this.computers, this.networkDevices, this.paymentDevices, this.mposDevices, this.otherAssets, this.printers);
            this.assetsLoaded = true;
            //this.onTypeChange(this.selectedType);
        }, true);
    }

    reloadLocations()
    {
        this.LocationService.RetrieveAll(data =>
        {
            this.locations = data;
            this.locationsLoaded = true;
        }, true);
    }

    loadLocations()
    {

        this.LocationService.RetrieveAll(data =>
        {
            this.locations = data;
            this.locationsLoaded = true;
        }, false);
    }

    filterAssets()
    {
        function filterComputers(element, index, array)
        {
            return element.assetType == "computer";
        }

        function filterNetworkDevices(element, index, array)
        {
            return element.assetType == "networkDevice";
        }

        function filterPaymentDevices(element, index, array)
        {
            return element.assetType == "paymentDevice";
        }

        function filterMposDevices(element, index, array)
        {
            return element.assetType == "mposDevice";
        }

        function filterOther(element, index, array)
        {
            return element.assetType == "other";

        }

        if(this.allAssets)
        {
            this.computers = this.allAssets.filter(filterComputers);
            this.networkDevices = this.allAssets.filter(filterNetworkDevices);
            this.paymentDevices = this.allAssets.filter(filterPaymentDevices);
            this.mposDevices = this.allAssets.filter(filterMposDevices);
            this.otherAssets = this.allAssets.filter(filterOther);
            this.assetsLoaded = true;
        }
    }

    onClickAdd()
    {
        let dialogRef = this.dialog.open(AssetDialogComponent,
            {
                height: '600px',
                width: '600px',
                data: {
                    asset: null,
                    edit: false,
                    locations: this.locations
                },
                disableClose: true
            });

        dialogRef.afterClosed().subscribe(result =>
        {
            if(result) this.reloadAssets();
            //this.selectedAsset = null;

        })
    }

    onClickEdit()
    {
        let dialogRef = this.dialog.open(AssetDialogComponent,
            {
                height: '600px',
                width: '600px',
                data:
                    {
                        asset: this.selectedAsset,
                        edit: true,
                        locations: this.locations
                    }
            });

        dialogRef.afterClosed().subscribe(result =>
        {
            if(result) this.reloadAssets();
            //this.selectedAsset = null;
            this.onRowSelect(this.selectedAsset);
        })
    }

    onClickDelete()
    {
        let dialogRef = this.dialog.open(ConfirmDialogComponent,
            {
                height: '200px',
                width: '300px'
            });

        dialogRef.afterClosed().subscribe(result =>
        {
            console.log('Dialog Result: ' + result);
            if(result) this.deleteAsset(this.selectedAsset);
        })
    }

    deleteAsset(asset: PhysicalAsset)
    {
        this.PhysicalAssetService.Delete(asset).subscribe(data =>
        {
            this.notificationService.showSnackBar("Asset Deleted", "Ok", 2000);
            this.reloadAssets();
        });
    }

/*    onTypeChange(newValue)
    {

        let value = newValue.value;
        this.selectedAsset = null;

        if(this.assetTableComponent)
            this.assetTableComponent.deselectRow();

        switch(value) {
            case(this.typesObject.computer.value):
            {
                this.currentAssets = this.computers;
                this.columns = this.computerColumns;
                this.columnsToDisplay = this.computerColumnsToDisplay;
                break;
            }

            case (this.typesObject.networkDevice.value):
            {
                this.currentAssets = this.networkDevices;
                this.columns = this.networkDeviceColumns;
                this.columnsToDisplay = this.networkColumnsToDisplay;
                break;
            }

            case (this.typesObject.paymentDevice.value):
            {
                this.currentAssets = this.paymentDevices;
                this.columns = this.paymentDeviceColumns;
                this.columnsToDisplay = this.paymentColumnsToDisplay;
                break;
            }

            case (this.typesObject.mposDevice.value):
            {
                this.currentAssets = this.mposDevices;
                this.columns = this.mposDeviceColumns;
                this.columnsToDisplay = this.mposColumnsToDisplay;
                break;
            }

            case(this.typesObject.other.value):
            {
                this.currentAssets = this.otherAssets;
                this.columns = this.otherColumns;
                this.columnsToDisplay = this.otherColumnsToDisplay;
                break;
            }

            default:
            {
                this.currentAssets = this.otherAssets;
                this.columns = this.otherColumns;
            }
        }

    }*/

    objectKeys = Object.keys;

    onRowSelect($row)
    {
        //console.log($row);
        this.selectedAsset = $row;
    }

}


@Pipe({name: 'byAssetType'})
export class ByAssetTypePipe implements PipeTransform
{
    transform(assets: PhysicalAsset[], type): any {

        if (!assets) return;
        let filteredAssets: PhysicalAsset[] = [];

        for (let i = 0; i < assets.length; i++)
        {
            if (assets[i].assetType == type.name)
            {
                filteredAssets.push(assets[i]);
            }
        }
        return filteredAssets;
    }
}