import { TestBed, inject } from '@angular/core/testing';

import { StoreResolverService } from './store-resolver.service';

describe('StoreResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StoreResolverService]
    });
  });

  it('should be created', inject([StoreResolverService], (service: StoreResolverService) => {
    expect(service).toBeTruthy();
  }));
});
