import {ServiceProvider} from "../service-provider/ServiceProvider";

export class Location {
    id: number;
    name: string;
    phone: string;
    address: string;
    downSpeed: number;
    upSpeed: number;
    hasStatic: boolean;
    external: string;
    hasRetailRadio: boolean;
    hasRingCentral: boolean;
    internetProviderName: string;
    internetProviderPhone: string;
    omniLocation: string;

    constructor()
    {
        this.id = 0;
        this.name = "";
        this.hasStatic = false;
        this.hasRetailRadio = false;
        this.hasRingCentral = false;

    }
}