import {Component, Input, OnInit, Pipe, PipeTransform} from '@angular/core';
import {Location} from "./Location";
import {ComputerService} from "../computers/computer.service";
import {PhysicalAsset} from "../physical-asset/PhysicalAsset";
import {Computer} from "../computers/Computer";
import {LocationService} from "../location.service";
import {PhysicalAssetService} from "../physical-asset/physical-asset.service";
import {Mpos} from "../mpos-device/Mpos";
import {NetworkDevice} from "../network-device/NetworkDevice";
import {MatDialog} from "@angular/material";
import {LocationDialogComponent} from "./location-dialog/location-dialog.component";
import {ServiceProvider} from "../service-provider/ServiceProvider";
import {ServiceProviderService} from "../service-provider/service-provider.service";

@Component({
    selector: 'app-store',
    templateUrl: './store.component.html',
    styleUrls: ['./store.component.scss']
})

export class StoreComponent implements OnInit {

    constructor(private ComputerService: ComputerService,
                private locationService: LocationService,
                private physicalAssetService: PhysicalAssetService,
                public dialog: MatDialog,) {
    }


    locations: Location[];
    selectedLocation = new Location();
    locationsLoaded = false;
    assetsLoaded = false;
    allComputers;
    allMpos;
    allNetworkDevices;
    allAssets;
    allPaymentDevices;
    allOther;
    allPrinters;
    downloadChart;
    uploadChart;
    selectedAsset;
    gaugeType;
    upGaugeLabel;
    gaugeAppendText;
    downGaugeLabel;
    foreground;


    computerColumns =
        [
            {
                columnDef: "assetNum",
                header: "Asset Number",
                cell: (row: Computer) => `${row.assetNum}`
            },
            {
                columnDef: "name",
                header: "Name",
                cell: (row: Computer) => `${row.name}`
            },
            {
                columnDef: "make",
                header: "Make",
                cell: (row: Computer) => `${row.make}`
            },
            {
                columnDef: "model",
                header: "Model",
                cell: (row: Computer) => `${row.model}`
            },
            {
                columnDef: "cpu",
                header: "CPU",
                cell: (row: Computer) => `${row.cpu}`
            },
            {
                columnDef: "memory",
                header: "Memory",
                cell: (row: Computer) => `${row.memory}`
            },
            {
                columnDef: "os",
                header: "OS",
                cell: (row: Computer) => `${row.os}`
            },
            {
                columnDef: "wifi",
                header: "WiFi",
                cell: (row: Computer) => `${row.wifi}`,
                isCheckbox: true
            }
        ];

    mposColumns =
        [
            {
                columnDef: "assetNum",
                header: "Asset Number",
                cell: (row: PhysicalAsset) => `${row.assetNum}`
            },
            {
                columnDef: "name",
                header: "Name",
                cell: (row: Mpos) => `${row.name}`
            },
            {
                columnDef: "make",
                header: "Make",
                cell: (row: PhysicalAsset) => `${row.make}`
            },
            {
                columnDef: "model",
                header: "Model",
                cell: (row: PhysicalAsset) => `${row.model}`
            }
        ];

    networkDeviceColumns =
        [
            {
                columnDef: "assetNum",
                header: "Asset Number",
                cell: (row: PhysicalAsset) => `${row.assetNum}`
            },
            {
                columnDef: "make",
                header: "Make",
                cell: (row: PhysicalAsset) => `${row.make}`
            },
            {
                columnDef: "model",
                header: "Model",
                cell: (row: PhysicalAsset) => `${row.model}`
            },
            {
                columnDef: "external",
                header: "External IP",
                cell: (row: NetworkDevice) => `${row.external}`
            },
            {
                columnDef: "internal",
                header: "Internal IP",
                cell: (row: NetworkDevice) => `${row.internal}`
            }
        ];

    paymentDeviceColumns =
        [
            {
                columnDef: "assetNum",
                header: "Asset Number",
                cell: (row: PhysicalAsset) => `${row.assetNum}`
            },
            {
                columnDef: "make",
                header: "Make",
                cell: (row: PhysicalAsset) => `${row.make}`
            },
            {
                columnDef: "model",
                header: "Model",
                cell: (row: PhysicalAsset) => `${row.model}`
            },
            {
                columnDef: "serial",
                header: "Serial",
                cell: (row: PhysicalAsset) => `${row.serial}`
            }
        ];

    ngOnInit()
    {
      /*  this.downloadChart = c3.generate({
            bindto: '#downloadGauge',
            data: {
                columns: [
                    ['data', 0],
                ],
                type: "gauge"

            },
            gauge:
                {
                    label:
                        {
                            format: function(value, ratio) {
                                return value + " mbps";
                            },
                            show: false // to turn off the min/max labels.
                        },
                    min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
                    max: 300, // 100 is default
                    units: ' mbps',
                    width: 30 // for adjusting arc thickness
                },
            color:
                {
                    pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
                    threshold:
                        {
                            unit: 'value', // percentage is default
                            max: 300, // 100 is default
                            values: [30, 60, 90, 100]
                        }
                },
            legend:
                {
                    hide: true
                },
            size:
                {
                    width: 150
                },
            padding:
                {
                    bottom: -20
                }
        });
        this.uploadChart = c3.generate({
            bindto: '#uploadGauge',
            data: {
                columns: [
                    ['data', 0],
                ],
                type: "gauge"

            },
            gauge:
                {
                    label:
                        {
                            format: function(value, ratio) {
                                return value + " mbps";
                            },
                            show: false // to turn off the min/max labels.
                        },
                    min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
                    max: 300, // 100 is default
                    units: ' mbps',
                    width: 30 // for adjusting arc thickness
                },
            color:
                {
                    pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
                    threshold:
                        {
                            unit: 'value', // percentage is default
                            max: 300, // 100 is default
                            values: [30, 60, 90, 100]
                        }
                },
            legend:
                {
                    hide: true
                },
            size:
                {
                    width: 150
                },
            padding:
                {
                    bottom: -20
                }
        });
*/

        this.gaugeType = "semi";
        this.downGaugeLabel = "Download Speed";
        this.gaugeAppendText = "mbps";
        this.upGaugeLabel = "Upload Speed";
        this.foreground = "rgba(0, 179, 0, 1)";


        this.loadLocations();
        this.loadAssets(true);
    }


    //calls RetrieveAll from service and subscribes to the observable, sets allAssets to data from that observable stream
    loadAssets(reload)
    {
        this.physicalAssetService.getAllAssets((data) =>
        {
            this.allAssets = data;
            //this.filterAssets();
            this.allComputers = this.allAssets.computers;
            this.allMpos = this.allAssets.mpos;
            this.allPaymentDevices = this.allAssets.paymentDevices;
            this.allNetworkDevices = this.allAssets.networkDevices;
            this.allOther = this.allAssets.other;
            this.allPrinters = this.allAssets.printers;
            this.assetsLoaded = true;
        }, reload);

    }

    loadLocations()
    {

        this.locationService.RetrieveAll(data =>
        {
            this.locations = data;

            if(this.locations.length < 1)
            {
                this.locationsLoaded = true;
                return;
            }
            this.selectedLocation = this.locations[0];
            this.locationsLoaded = true;
            //this.downloadChart.load({columns: [['data', this.selectedLocation.downSpeed]]});
            //this.uploadChart.load({columns: [['data', this.selectedLocation.upSpeed]]})
        }, false);
    }

    reloadLocations()
    {
        this.locationService.RetrieveAll(data =>
        {
            this.locations = data;

            if(this.locations.length < 1)
            {
                this.locationsLoaded = true;
                return;
            }
            this.selectedLocation = this.locations[0];
            this.locationsLoaded = true;
           // this.downloadChart.load({columns: [['data', this.selectedLocation.downSpeed]]});
            //this.uploadChart.load({columns: [['data', this.selectedLocation.upSpeed]]})
        }, true);
    }


    onClickEditLocation()
    {
        let dialogRef = this.dialog.open(LocationDialogComponent,
            {
                height: '800px',
                width: '450px',
                data:
                    {
                        location: this.selectedLocation,
                        isEdit: true
                }
            });

        dialogRef.afterClosed().subscribe(result =>
        {
            if(result) this.reloadLocations();
            //this.selectedAsset = null;

        })
    }

    onClickAddLocation()
    {
        let dialogRef = this.dialog.open(LocationDialogComponent,
            {
                height: '450px',
                width: '400px',
                data:
                    {
                        location: new Location(),
                        isEdit: false
                    }
            });

        dialogRef.afterClosed().subscribe(result =>
        {
            if(result) this.reloadLocations();
            //this.selectedAsset = null;

        })
    }

    onRowSelect($row)
    {
        //console.log($row);
        this.selectedAsset = $row;
    }
}

@Pipe({name: 'byLocation'})
export class ByLocationPipe implements PipeTransform
{
    transform(assets: PhysicalAsset[], location: Location): any {

        if (!assets) return;

        let filteredAssets: PhysicalAsset[] = [];

        for (let i = 0; i < assets.length; i++)
        {
            if (assets[i].location.name == location.name)
            {
                filteredAssets.push(assets[i]);
            }
        }
        return filteredAssets;
    }
}