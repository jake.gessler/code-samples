import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {source} from "@angular-devkit/schematics";
import {Location} from "../../store/Location";
import {LocationService} from "../../location.service";
import {AssetDialogComponent} from "../../inventory/asset-dialog/asset-dialog.component";
import {NotificationService} from "../../notification-service/notification.service";
import {ServiceProviderService} from "../../service-provider/service-provider.service";
import {ServiceProvider} from "../../service-provider/ServiceProvider";

@Component({
    selector: 'app-location-dialog',
    templateUrl: './location-dialog.component.html',
    styleUrls: ['./location-dialog.component.scss']
})
export class LocationDialogComponent implements OnInit {


    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private LocationService: LocationService,
        public dialogRef: MatDialogRef<LocationDialogComponent>,
        private notificationService: NotificationService,
        private serviceProviderService: ServiceProviderService
    ) {}

    isEdit;
    curLocation;
    tempLocation = new Location();
    isLoading = false;

    ngOnInit()
    {
        this.isEdit = this.data.isEdit;
        this.curLocation = this.data.location;

        //Check if a location was passed in (for edit) and create a temp copy to edit
        if (this.curLocation)
        {
            this.tempLocation = JSON.parse(JSON.stringify(this.curLocation));
        }


    }

    onSave()
    {
        this.isLoading = true;

        this.LocationService.Update(this.tempLocation).subscribe(response =>
        {
            this.isLoading = false;
            let message = this.isEdit ? "Location Updated" : "Location Added";
            this.notificationService.showSnackBar(message, "OK", 2000);
            console.log(response);
            this.dialogRef.close(true);
        });
    }


}
