import {Injectable, NgZone} from '@angular/core';
import {Router} from "@angular/router";
import {BehaviorSubject} from "rxjs";
import {User} from "../users/User";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {NotificationService} from "../notification-service/notification.service";

@Injectable()
export class AuthenticationService {

    private loggedIn = new BehaviorSubject<boolean>(false); // {1
    private apiUrl: string;

    get isLoggedInObservable() {
        return this.loggedIn.asObservable(); // {2}
    }

    get loggedInSubject()
    {
        return this.loggedIn;
    }

    constructor(
        private router: Router,
        private http: HttpClient,
        private zone: NgZone,
        private notificationService: NotificationService
    )
    {
        this.apiUrl = environment.apiUrl;
    }

    isLoggedIn() : boolean
    {
       return this.getLoggedInUser() !== null;
    }

    getLoggedInUser()
    {
        return localStorage.getItem("LoggedInUser");
    }

    login(user: User){

        let observable = this.http.post<User>( this.apiUrl + "login", user);

        observable.subscribe(data => {
            if(data) {
                localStorage.setItem("LoggedInUser", data.username.toString());
                this.loggedIn.next(true);
                this.router.navigate(['/assets']);
            }
            else
            {
                this.loggedIn.next(false);
                this.notificationService.showSnackBar("Invalid Username or Password", "OK", 2000);
            }


        });

       // this.loggedIn.next(true);

/*        if (user.username !== '' && user.password != '' )
        { // {3}
            this.loggedIn.next(true);
            this.router.navigate(['/']);s
        }*/
    }

    logout() {                            // {4}
        this.loggedIn.next(false);
        this.router.navigate(['/login']);
        localStorage.removeItem("LoggedInUser");

        let observable = this.http.get(this.apiUrl + "logout");

        observable.subscribe(data => {
            console.log(data);

        });
    }

    logoutFromErrorHandler()
    {
        this.zone.run(() => this.logout());
    }
}
