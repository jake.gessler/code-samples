import { Injectable } from '@angular/core';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthenticationService} from "./authentication.service";
import "rxjs/add/operator/take";
import "rxjs/add/operator/map";
//import { take } from 'rxjs/operators';

@Injectable()
export class AuthenticationGuard implements CanActivate {

    constructor(
        private authService: AuthenticationService,
        private router: Router
    ) {}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>
    {
        return this.authService.isLoggedInObservable
            .take(1)
            .map((isLoggedIn: boolean) => {
                if (!isLoggedIn){
                    this.router.navigate(['/login']);
                    return false;
                }
                return true;
        });
    }
}
