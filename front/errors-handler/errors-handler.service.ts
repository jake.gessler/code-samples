import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {MatSnackBar} from "@angular/material";
import {NotificationService} from "../notification-service/notification.service";
import {Router} from "@angular/router";
import {AuthenticationService} from "../authentication/authentication.service";

@Injectable()
export class ErrorsHandlerService implements ErrorHandler {

    constructor(private injector: Injector)
    {

    }



    handleError(error: any): void {

        const notificationService = this.injector.get(NotificationService);
        const authenticationService = this.injector.get(AuthenticationService);

        if(error.status == 401)
        {
            notificationService.showSnackBar("Please Sign In", "Ok", 2000);
            authenticationService.logoutFromErrorHandler();
        }

        else
        {
            console.error('It happens: ', error);
            notificationService.showSnackBar("Oops! An error occurred", "Ok", 2000);
        }

    }


}
