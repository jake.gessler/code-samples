import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Location} from "./store/Location";
import {environment} from "../environments/environment";

@Injectable()
export class LocationService {

    private allLocations: Location[];
    private apiUrl: String;

    constructor(private http: HttpClient)
    {
        this.allLocations = [];
        this.apiUrl = environment.apiUrl;
    }

    RetrieveAll(callback, reload)
    {
        if (this.allLocations.length == 0 || this.allLocations === undefined || reload == true)
            this.RetrieveAllFromServer().subscribe( data => {
                callback(data);
                this.allLocations = data;
                return;
            });

        callback(this.allLocations);
    }

    RetrieveAllFromServer()
    {
        return this.http.get<Location[]>(this.apiUrl + "location/retrieve");
    }

    Update(location: Location)
    {
        return this.http.post<Location[]>(this.apiUrl + "location/update", location);
    }

    Delete(location: Location)
    {
        return this.http.post<Location[]>(this.apiUrl + "location/delete", location);
    }


}
