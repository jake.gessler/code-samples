import {Injectable} from '@angular/core';
import {Computer} from "./Computer";
import {PhysicalAsset} from "../physical-asset/PhysicalAsset";
import { Location } from "../store/Location";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable()
export class ComputerService {

    private apiUrl: String;

    constructor(private http: HttpClient) {
        this.apiUrl = environment.apiUrl;
    }

   //Snipe-IT version
   /*   allComputers() {
        return this.http.get<Computer[]>("api/computer/retrieveAllSnipe");
    }*/

    allComputers() {
        return this.http.get<Computer[]>(this.apiUrl + "computer/retrieveAllSnipe");
    }

    computersByLocation() {

    }


    Update(computer: Computer)
    {

       return this.http.post(this.apiUrl + "computer/update", computer);

     /*   observable.subscribe(data => {
            console.log(data);

        });*/
    }

}
