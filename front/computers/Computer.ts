import {PhysicalAsset} from "../physical-asset/PhysicalAsset";

export class Computer extends PhysicalAsset {
    name: string;
    cpu: string;
    memory: string;
    storage: string;
    os: string;
    wifi: number;


        constructor()
        {
            super();
            this.wifi = 0;
        }
}