import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {Computer} from "./Computer";

@Component({
    selector: 'app-computers',
    templateUrl: './computers.component.html',
    styleUrls: ['./computers.component.scss']
})
export class ComputersComponent implements OnInit {

    constructor() {
    }

    @Input() computers: Computer[];
    @Input() showLocation: boolean;
    @Output() selectRowEvent = new EventEmitter();

    columnsToDisplay = ['name', 'assetNum', "make", "model", "cpu", "mem", "os", "wifi"];

    selectedRowIndex = -1;

    ngOnInit()
    {
        if(this.showLocation)
            this.columnsToDisplay.unshift('location');
    }



    selectRow(row)
    {
        if (this.selectedRowIndex==row.id)
        {
            this.selectedRowIndex = -1;
            this.selectRowEvent.emit(null);
            return;
        }
        this.selectRowEvent.emit(row);
        this.selectedRowIndex = row.id;
    }


}
